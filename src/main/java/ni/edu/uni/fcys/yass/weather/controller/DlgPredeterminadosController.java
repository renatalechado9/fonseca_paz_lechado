/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.fcys.yass.weather.controller;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Arrays;
import java.util.List;
import javax.swing.DefaultListModel;
import ni.edu.uni.fcys.yass.weather.pojo.City;

/**
 *
 * @author Hp
 */
public class DlgPredeterminadosController {

    public DlgPredeterminadosController() {
    }

    
    
    private List<City> getUltimosList() throws FileNotFoundException {
        Gson gson = new Gson();
        JsonReader reader
                = new JsonReader(new FileReader(getClass().getResource("/ni/edu/uni/fcys/yass/predeterminados.json").getPath()));
        City[] data = gson.fromJson(reader, City[].class);

        return Arrays.asList(data);
    }

    public DefaultListModel getListModel() throws FileNotFoundException {
        DefaultListModel listModel = new DefaultListModel();
        getUltimosList().forEach((city) -> {
            listModel.addElement(city.toString());
        });
        return listModel;
    }
}
