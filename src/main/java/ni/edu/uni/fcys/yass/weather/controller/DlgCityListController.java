/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.fcys.yass.weather.controller;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.text.Normalizer;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import javax.swing.DefaultListModel;
import ni.edu.uni.fcys.yass.weather.pojo.City;

/**
 *
 * @author Yasser
 */
public class DlgCityListController {

    public DlgCityListController() {
    }

    private List<City> getCityList() throws FileNotFoundException {
        Gson gson = new Gson();
        JsonReader reader
                = new JsonReader(new FileReader(getClass().getResource("/ni/edu/uni/fcys/yass/city.list.json").getPath()));
        City[] data = gson.fromJson(reader, City[].class);

        for (int i = 0; i < data.length; i++) {
            City ciudad = data[i];
            String result = remove2(ciudad.getName());
            ciudad.setName(result);
            data[i] = ciudad;
        }

        return Arrays.asList(data);
    }
      private String remove2(String input) {
        // Descomposición canónica
        String normalized = Normalizer.normalize(input, Normalizer.Form.NFD);
        // Nos quedamos únicamente con los caracteres ASCII
        Pattern pattern = Pattern.compile("\\P{ASCII}");
        return pattern.matcher(normalized).replaceAll("");
    }

    public DefaultListModel getListModel() throws FileNotFoundException {
        DefaultListModel listModel = new DefaultListModel();
        getCityList().parallelStream()
                .filter((city) -> city.getCountry().equals("NI"))
                .forEach((city) -> {
                    listModel.addElement(city.toString());
                });
        return listModel;
    }
}
